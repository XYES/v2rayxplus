package ui

import (
	"github.com/fsnotify/fsnotify"
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/xiayesuifeng/v2rayxplus/conf"
	core2 "gitlab.com/xiayesuifeng/v2rayxplus/core"
	"gitlab.com/xiayesuifeng/v2rayxplus/styles"
	"gitlab.com/xiayesuifeng/v2rayxplus/ui/configEdit"
	widgets2 "gitlab.com/xiayesuifeng/v2rayxplus/ui/widgets"
	"io/ioutil"
	"log"
	"path/filepath"
)

type MainFrame struct {
	*widgets.QFrame

	hboxLayout *widgets.QHBoxLayout

	configList *widgets2.ConfigList

	startButton   *widgets.QPushButton
	settingButton *widgets.QPushButton

	configEdit *configEdit.ConfigEdit

	groupComboBox *widgets.QComboBox

	configName string
}

func NewMainFrame(parent widgets.QWidget_ITF, fo core.Qt__WindowType) *MainFrame {
	frame := &MainFrame{QFrame: widgets.NewQFrame(parent, fo)}

	frame.init()
	frame.initWatcher()
	frame.initConnect()

	frame.groupComboBox.SetCurrentTextDefault("default")

	return frame
}

func (m *MainFrame) init() {
	m.hboxLayout = widgets.NewQHBoxLayout2(m)
	m.hboxLayout.SetSpacing(0)
	m.hboxLayout.SetContentsMargins(0, 0, 0, 0)

	vboxLayout := widgets.NewQVBoxLayout2(m)

	m.configList = widgets2.NewConfigList(m, 0)

	m.startButton = widgets.NewQPushButton(m)
	m.startButton.SetFixedSize2(230, 230)
	if exited, _ := core2.StatusV2rayXPlusSerive(); exited {
		m.startButton.SetStyleSheet(styles.GetStyleSheet(styles.StopButton))
		m.startButton.SetWindowTitle("on")
	} else {
		m.startButton.SetStyleSheet(styles.GetStyleSheet(styles.StartButton))
	}

	m.settingButton = widgets.NewQPushButton(m)
	m.settingButton.SetFixedSize2(24, 24)
	m.settingButton.SetStyleSheet(styles.GetStyleSheet(styles.SettingButton))

	m.groupComboBox = widgets.NewQComboBox(m)
	if infos, err := ioutil.ReadDir(conf.V2rayConfigPath); err == nil {
		items := make([]string, 0)
		for _, info := range infos {
			if info.IsDir() {
				items = append(items, info.Name())
			}
		}
		m.groupComboBox.AddItems(items)
	}

	hboxLayout := widgets.NewQHBoxLayout2(m)
	hboxLayout.AddWidget(m.settingButton, 0, core.Qt__AlignLeft|core.Qt__AlignBottom)
	hboxLayout.AddWidget(m.groupComboBox, 0, core.Qt__AlignRight|core.Qt__AlignBottom)

	vboxLayout.AddSpacing(60)
	vboxLayout.AddWidget(m.startButton, 0, core.Qt__AlignHCenter)
	vboxLayout.AddSpacing(48)
	var versionLabel *widgets.QLabel
	if version, err := core2.GetVension(); err != nil {
		versionLabel = widgets.NewQLabel2("v2ray版本: 未安装", m, 0)
	} else {
		versionLabel = widgets.NewQLabel2("v2ray版本: "+version, m, 0)
	}

	vboxLayout.AddWidget(versionLabel, 0, core.Qt__AlignHCenter)
	vboxLayout.AddWidget(m.configList, 1, core.Qt__AlignBottom)
	vboxLayout.AddSpacing(20)
	vboxLayout.AddLayout(hboxLayout, 0)

	m.configEdit = configEdit.NewConfigEdit(m, 0)

	m.hboxLayout.AddLayout(vboxLayout, 0)
	m.hboxLayout.AddWidget(m.configEdit, 0, core.Qt__AlignRight)

	m.SetLayout(m.hboxLayout)
}

func (m *MainFrame) initConnect() {
	m.startButton.ConnectClicked(func(checked bool) {
		if m.startButton.WindowTitle() == "on" {
			if core2.StopV2rayXPlusSerive() {
				m.startButton.SetWindowTitle("off")
				m.startButton.SetStyleSheet(styles.GetStyleSheet(styles.StartButton))
			}
		} else {
			if core2.StartV2rayXPlusSerive(m.configList.ConfigName, m.groupComboBox.CurrentText()) {
				m.startButton.SetWindowTitle("on")
				m.startButton.SetStyleSheet(styles.GetStyleSheet(styles.StopButton))
			}
		}
	})

	m.configList.ConnectConfigChange(func(name string) {
		m.configEdit.ConfigChange(name, m.groupComboBox.CurrentText())
	})

	m.configList.ConnectConfigChange(func(name string) {
		if m.startButton.WindowTitle() == "on" {
			if name != m.configName {
				core2.RestartV2rayXPlusSerive(name, m.groupComboBox.CurrentText())
			}
			m.configName = name
		}
	})

	m.configList.ConnectEditConfig(func(name string) {
		x := m.ParentWidget().Geometry().X()
		y := m.ParentWidget().Geometry().Y()

		left := x - (1050-m.ParentWidget().Width())/2

		m.ParentWidget().SetMaximumSize(core.NewQSize2(1050, 600))

		animation := core.NewQPropertyAnimation2(m.Parent(), core.NewQByteArray2("geometry", 8), m)
		animation.SetDuration(200)
		animation.SetStartValue(core.NewQVariant1(core.NewQRect4(x, y, 350, 600)))
		animation.SetEndValue(core.NewQVariant1(core.NewQRect4(left, y, 1050, 600)))
		animation.ConnectFinished(func() {
			m.ParentWidget().SetMinimumSize(core.NewQSize2(1050, 600))
			m.configEdit.EditChange(name, m.groupComboBox.CurrentText())
		})

		animation.Start(core.QAbstractAnimation__KeepWhenStopped)
	})

	m.configList.ConnectRemoveConfig(func(name string) {
		if m.configName == name {
			core2.StopV2rayXPlusSerive()
		}
	})

	m.settingButton.ConnectClicked(func(checked bool) {
		NewSettingFrame(nil, 0).Show()
	})

	m.groupComboBox.ConnectCurrentTextChanged(m.configList.GroupChange)
}

func (m *MainFrame) initWatcher() {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Printf("create watching failed: %s", err.Error())
	} else {
		go func() {
			for {
				select {
				case event, ok := <-watcher.Events:
					if !ok {
						return
					}
					if event.Op&fsnotify.Create == fsnotify.Create {
						m.groupComboBox.AddItems([]string{filepath.Base(event.Name)})
					} else if event.Op&fsnotify.Remove == fsnotify.Remove {
						group := filepath.Base(event.Name)
						if group == m.groupComboBox.CurrentText() {
							m.groupComboBox.SetCurrentText("default")
						}
						m.groupComboBox.RemoveItem(m.groupComboBox.FindText(group, core.Qt__MatchExactly))
					}
				}
			}
		}()

		if err = watcher.Add(conf.V2rayConfigPath); err != nil {
			log.Printf("watching %s failed", err.Error())
		}
	}
}
