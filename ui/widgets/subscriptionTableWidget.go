package widgets

import (
	"fmt"
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/xiayesuifeng/v2rayxplus/conf"
	"gitlab.com/xiayesuifeng/v2rayxplus/subscription"
)

type SubscriptionTableWidget struct {
	widgets.QWidget

	tableView *widgets.QTableView
	itemModel *SubscriptionTableModel

	addButton    *widgets.QPushButton
	deleteButton *widgets.QPushButton
	updateButton *widgets.QPushButton

	updateMessageBox *widgets.QMessageBox

	adapters []string

	_ func()            `constructor:"init"`
	_ func(text string) `signal:"updateMessageBoxText"`
	_ func()            `signal:"updateMessageBoxButton"`
}

func (ptr *SubscriptionTableWidget) init() {
	vboxLayout := widgets.NewQVBoxLayout2(ptr)

	ptr.adapters = subscription.GetAdapterNames()

	ptr.updateMessageBox = widgets.NewQMessageBox(ptr)
	ptr.updateMessageBox.SetWindowTitle("更新订阅")
	ptr.updateMessageBox.SetWindowFlag(core.Qt__WindowCloseButtonHint, false)
	ptr.updateMessageBox.SetStandardButtons(widgets.QMessageBox__NoButton)

	ptr.tableView = widgets.NewQTableView(ptr)
	ptr.tableView.HorizontalHeader().SetSectionResizeMode(widgets.QHeaderView__Stretch)
	ptr.tableView.VerticalHeader().SetHidden(true)
	ptr.tableView.SetSelectionBehavior(widgets.QAbstractItemView__SelectRows)
	ptr.tableView.SetSelectionMode(widgets.QAbstractItemView__SingleSelection)
	ptr.tableView.SetItemDelegateForColumn(2, NewSubscriptionTableDelegate(ptr))

	ptr.itemModel = NewSubscriptionTableModel(ptr)

	ptr.tableView.SetModel(ptr.itemModel)

	hboxLayout := widgets.NewQHBoxLayout2(ptr)

	ptr.addButton = widgets.NewQPushButton2("添加", ptr)
	ptr.deleteButton = widgets.NewQPushButton2("删除", ptr)
	ptr.updateButton = widgets.NewQPushButton2("更新订阅", ptr)

	ptr.deleteButton.SetEnabled(false)

	hboxLayout.AddStretch(1)
	hboxLayout.AddWidget(ptr.updateButton, 0, core.Qt__AlignCenter)
	hboxLayout.AddWidget(ptr.addButton, 0, core.Qt__AlignCenter)
	hboxLayout.AddWidget(ptr.deleteButton, 0, core.Qt__AlignCenter)
	hboxLayout.AddStretch(1)

	vboxLayout.AddWidget(ptr.tableView, 1, 0)
	vboxLayout.AddLayout(hboxLayout, 0)
	ptr.SetLayout(vboxLayout)

	ptr.initConnect()
}

func (ptr *SubscriptionTableWidget) initConnect() {
	ptr.tableView.ConnectCurrentChanged(func(current *core.QModelIndex, previous *core.QModelIndex) {
		ptr.deleteButton.SetEnabled(true)
	})

	ptr.addButton.ConnectClicked(func(checked bool) {
		adapter := ""
		if len(ptr.adapters) > 0 {
			adapter = ptr.adapters[0]
		}
		ptr.itemModel.add(conf.Subscription{Group: "", URL: "", Adapter: adapter})
	})

	ptr.deleteButton.ConnectClicked(func(checked bool) {
		indexes := ptr.tableView.SelectedIndexes()
		if len(indexes) > 0 {
			ptr.itemModel.remove(indexes[0].Row())
		}
	})

	ptr.updateButton.ConnectClicked(func(checked bool) {
		total := len(ptr.itemModel.modelData)
		ptr.updateMessageBox.SetInformativeText(fmt.Sprintf("更新中 0/%d...", total))
		go func() {
			var result string

			for i, data := range ptr.itemModel.modelData {
				ptr.UpdateMessageBoxText(fmt.Sprintf("更新中 %d/%d...", i, total))
				totalCount, availableCount, err := subscription.UpdateSubscription(data.Group, data.URL, data.Adapter)
				if err != nil {
					result += fmt.Sprintf("%s 订阅错误: %s", data.Group, err)
				} else {
					result += fmt.Sprintf("%s 订阅完成: 总数: %d, 可用: %d", data.Group, totalCount, availableCount)
				}
				if i < total {
					result += "\n"
				}
			}

			ptr.UpdateMessageBoxText(result)
			ptr.UpdateMessageBoxButton()
		}()
		ptr.updateMessageBox.Show()
	})

	ptr.ConnectUpdateMessageBoxText(func(text string) {
		ptr.updateMessageBox.SetInformativeText(text)
	})

	ptr.ConnectUpdateMessageBoxButton(func() {
		ptr.updateMessageBox.AddButton3(widgets.QMessageBox__Ok)
	})
}

func (ptr *SubscriptionTableWidget) LoadData(subscriptions []conf.Subscription) {
	for _, s := range subscriptions {
		ptr.itemModel.add(s)
	}
}

func (ptr *SubscriptionTableWidget) Data() (subscriptions []conf.Subscription) {
	for _, s := range ptr.itemModel.modelData {
		if s.Group == "" || s.Adapter == "" {
			continue
		}
		subscriptions = append(subscriptions, s)
	}
	return
}
