package widgets

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
)

type HostsTableWidget struct {
	widgets.QWidget

	tableView *widgets.QTableView
	itemModel *HostsTableModel

	addButton    *widgets.QPushButton
	copyButton   *widgets.QPushButton
	deleteButton *widgets.QPushButton

	_ func() `constructor:"init"`
}

func (ptr *HostsTableWidget) init() {
	hboxLayout := widgets.NewQHBoxLayout2(ptr)

	ptr.tableView = widgets.NewQTableView(ptr)
	ptr.tableView.HorizontalHeader().SetSectionResizeMode(widgets.QHeaderView__Stretch)
	ptr.tableView.VerticalHeader().SetHidden(true)
	ptr.tableView.SetSelectionBehavior(widgets.QAbstractItemView__SelectRows)
	ptr.tableView.SetSelectionMode(widgets.QAbstractItemView__SingleSelection)

	ptr.itemModel = NewHostsTableModel(ptr)

	ptr.tableView.SetModel(ptr.itemModel)

	vboxLayout := widgets.NewQVBoxLayout2(ptr)

	ptr.addButton = widgets.NewQPushButton2("添加", ptr)
	ptr.copyButton = widgets.NewQPushButton2("复制", ptr)
	ptr.deleteButton = widgets.NewQPushButton2("删除", ptr)

	ptr.copyButton.SetEnabled(false)
	ptr.deleteButton.SetEnabled(false)

	vboxLayout.AddWidget(ptr.addButton, 0, core.Qt__AlignTop)
	vboxLayout.AddWidget(ptr.copyButton, 0, core.Qt__AlignTop)
	vboxLayout.AddWidget(ptr.deleteButton, 0, core.Qt__AlignTop)
	vboxLayout.AddStretch(1)

	hboxLayout.AddWidget(ptr.tableView, 1, 0)
	hboxLayout.AddLayout(vboxLayout, 0)
	ptr.SetLayout(hboxLayout)

	ptr.initConnect()
}

func (ptr *HostsTableWidget) initConnect() {
	ptr.tableView.ConnectCurrentChanged(func(current *core.QModelIndex, previous *core.QModelIndex) {
		ptr.deleteButton.SetEnabled(true)
		ptr.copyButton.SetEnabled(true)
	})

	ptr.addButton.ConnectClicked(func(checked bool) {
		ptr.itemModel.add(HostsTableItem{Host: "", IP: ""})
	})

	ptr.deleteButton.ConnectClicked(func(checked bool) {
		indexes := ptr.tableView.SelectedIndexes()
		if len(indexes) > 0 {
			ptr.itemModel.remove(indexes[0].Row())
		}
	})

	ptr.copyButton.ConnectClicked(func(checked bool) {
		indexes := ptr.tableView.SelectedIndexes()
		if len(indexes) > 0 {
			ptr.itemModel.copy(indexes[0].Row())
		}
	})
}

func (ptr *HostsTableWidget) LoadData(hosts map[string]string) {
	for host, ip := range hosts {
		ptr.itemModel.add(HostsTableItem{Host: host, IP: ip})
	}
}

func (ptr *HostsTableWidget) Data() map[string]string {
	hosts := make(map[string]string)
	for _, item := range ptr.itemModel.modelData {
		hosts[item.Host] = item.IP
	}

	return hosts
}
