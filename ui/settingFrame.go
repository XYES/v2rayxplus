package ui

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/xiayesuifeng/v2rayxplus/conf"
	widgets2 "gitlab.com/xiayesuifeng/v2rayxplus/ui/widgets"
	"strconv"
	"strings"
	"unicode"
)

var outboundTag = []string{"direct", "proxy", "block"}

type SettingFrame struct {
	*widgets.QFrame

	themeComboBox  *widgets.QComboBox
	tproxyComboBox *widgets.QComboBox
	hijackDNS      *widgets.QCheckBox

	adsComboBox  *widgets.QComboBox
	btComboBox   *widgets.QComboBox
	ipComboBox   *widgets.QComboBox
	siteComboBox *widgets.QComboBox

	portEdit             *widgets.QLineEdit
	iptablesDirectIPEdit *widgets.QPlainTextEdit

	domainWhitelistEdit *widgets.QPlainTextEdit
	domainBlacklistEdit *widgets.QPlainTextEdit

	ipWhitelistEdit *widgets.QPlainTextEdit
	ipBlacklistEdit *widgets.QPlainTextEdit

	hostTableWidget         *widgets2.HostsTableWidget
	subscriptionTableWidget *widgets2.SubscriptionTableWidget

	dnsListWidget *widgets2.DNSListWidget

	saveButton   *widgets.QPushButton
	cancelButton *widgets.QPushButton
}

func NewSettingFrame(parent widgets.QWidget_ITF, fo core.Qt__WindowType) *SettingFrame {
	frame := widgets.NewQFrame(parent, fo)

	settingFrame := &SettingFrame{QFrame: frame}
	settingFrame.init()
	settingFrame.initConnect()

	return settingFrame
}

func (ptr *SettingFrame) init() {
	ptr.SetWindowTitle("设置")

	vboxLayout := widgets.NewQVBoxLayout2(ptr)

	tabWidget := widgets.NewQTabWidget(ptr)

	baseWidget := widgets.NewQWidget(ptr, 0)
	baseLayout := widgets.NewQFormLayout(baseWidget)

	ptr.themeComboBox = widgets.NewQComboBox(ptr)
	ptr.themeComboBox.AddItems([]string{"light", "dark"})
	ptr.themeComboBox.SetCurrentText(conf.Conf.Theme)

	baseLayout.AddRow3("主题:", ptr.themeComboBox)

	transparentProxyWidget := widgets.NewQWidget(ptr, 0)
	transparentProxyLayout := widgets.NewQFormLayout(transparentProxyWidget)

	ptr.tproxyComboBox = widgets.NewQComboBox(ptr)
	ptr.tproxyComboBox.AddItems([]string{"redirect", "tproxy"})
	ptr.tproxyComboBox.SetCurrentText(conf.Conf.TProxy)

	ptr.portEdit = widgets.NewQLineEdit2(strconv.FormatInt(int64(conf.Conf.ListerPort), 10), ptr)
	ptr.portEdit.SetValidator(gui.NewQIntValidator2(1024, 65535, ptr))

	ptr.iptablesDirectIPEdit = widgets.NewQPlainTextEdit2(strings.Join(conf.Conf.IptablesDirectIPList, "\n"), ptr)
	ptr.iptablesDirectIPEdit.SetMinimumHeight(72)

	transparentProxyLayout.AddRow3("代理方式:", ptr.tproxyComboBox)
	transparentProxyLayout.AddRow3("监听端口:", ptr.portEdit)
	transparentProxyLayout.AddRow3("直连IP:", ptr.iptablesDirectIPEdit)

	routerScrollArea := widgets.NewQScrollArea(ptr)
	routerWidget := widgets.NewQWidget(routerScrollArea, 0)
	routerScrollLayout := widgets.NewQFormLayout(routerWidget)

	routerScrollArea.SetWidget(routerWidget)
	routerScrollArea.SetWidgetResizable(true)

	ptr.adsComboBox = widgets.NewQComboBox(ptr)
	ptr.btComboBox = widgets.NewQComboBox(ptr)
	ptr.ipComboBox = widgets.NewQComboBox(ptr)
	ptr.siteComboBox = widgets.NewQComboBox(ptr)

	ptr.adsComboBox.AddItems(outboundTag)
	ptr.btComboBox.AddItems(outboundTag)
	ptr.ipComboBox.AddItems(outboundTag)
	ptr.siteComboBox.AddItems(outboundTag)

	ptr.adsComboBox.SetCurrentText(conf.Conf.AdsOutboundTag)
	ptr.btComboBox.SetCurrentText(conf.Conf.BTOutboundTag)
	ptr.ipComboBox.SetCurrentText(conf.Conf.IPOutboundTag)
	ptr.siteComboBox.SetCurrentText(conf.Conf.SiteOutboundTag)

	ptr.domainWhitelistEdit = widgets.NewQPlainTextEdit2(strings.Join(conf.Conf.DomainWhitelist, ",\n"), ptr)
	ptr.domainWhitelistEdit.SetMinimumHeight(72)

	ptr.domainBlacklistEdit = widgets.NewQPlainTextEdit2(strings.Join(conf.Conf.DomainBlacklist, ",\n"), ptr)
	ptr.domainBlacklistEdit.SetMinimumHeight(72)

	ptr.ipWhitelistEdit = widgets.NewQPlainTextEdit2(strings.Join(conf.Conf.IPWhitelist, ",\n"), ptr)
	ptr.ipWhitelistEdit.SetMinimumHeight(72)

	ptr.ipBlacklistEdit = widgets.NewQPlainTextEdit2(strings.Join(conf.Conf.IPBlacklist, ",\n"), ptr)
	ptr.ipBlacklistEdit.SetMinimumHeight(72)

	routerScrollLayout.AddRow3("广告拦截", ptr.adsComboBox)
	routerScrollLayout.AddRow3("BT 流量", ptr.btComboBox)
	routerScrollLayout.AddRow3("国内IP和私有IP", ptr.ipComboBox)
	routerScrollLayout.AddRow3("国内域名", ptr.siteComboBox)
	routerScrollLayout.AddRow3("域名白名单", ptr.domainWhitelistEdit)
	routerScrollLayout.AddRow3("域名黑名单", ptr.domainBlacklistEdit)
	routerScrollLayout.AddRow3("IP白名单", ptr.ipWhitelistEdit)
	routerScrollLayout.AddRow3("IP黑名单", ptr.ipBlacklistEdit)

	dnsWidget := widgets.NewQWidget(ptr, 0)
	dnsLayout := widgets.NewQFormLayout(dnsWidget)

	ptr.hostTableWidget = widgets2.NewHostsTableWidget(ptr, 0)
	ptr.hostTableWidget.LoadData(conf.Conf.Hosts)

	ptr.dnsListWidget = widgets2.NewDNSListWidget(ptr, 0)
	ptr.dnsListWidget.LoadData(conf.Conf.DnsServers)

	ptr.hijackDNS = widgets.NewQCheckBox2("劫持本机DNS", ptr)
	ptr.hijackDNS.SetChecked(conf.Conf.HijackDNS)

	dnsLayout.AddRow3("Host", ptr.hostTableWidget)
	dnsLayout.AddRow3("DNS服务器:", ptr.dnsListWidget)
	dnsLayout.AddRow5(ptr.hijackDNS)

	ptr.subscriptionTableWidget = widgets2.NewSubscriptionTableWidget(ptr, 0)
	ptr.subscriptionTableWidget.LoadData(conf.Conf.Subscriptions)

	tabWidget.AddTab(baseWidget, "基本")
	tabWidget.AddTab(transparentProxyWidget, "透明代理")
	tabWidget.AddTab(routerScrollArea, "路由")
	tabWidget.AddTab(dnsWidget, "DNS")
	tabWidget.AddTab(ptr.subscriptionTableWidget, "订阅")

	hboxLayout := widgets.NewQHBoxLayout2(ptr)

	ptr.saveButton = widgets.NewQPushButton2("保存", ptr)
	ptr.cancelButton = widgets.NewQPushButton2("取消", ptr)

	hboxLayout.AddStretch(1)
	hboxLayout.AddWidget(ptr.saveButton, 0, core.Qt__AlignRight)
	hboxLayout.AddWidget(ptr.cancelButton, 0, core.Qt__AlignRight)

	vboxLayout.AddWidget(tabWidget, 1, 0)
	vboxLayout.AddLayout(hboxLayout, 0)

	ptr.SetLayout(vboxLayout)
}

func (ptr *SettingFrame) initConnect() {
	ptr.saveButton.ConnectClicked(ptr.saveButtonClicked)

	ptr.cancelButton.ConnectClicked(func(checked bool) {
		ptr.Close()
	})
}

func (ptr *SettingFrame) saveButtonClicked(checked bool) {
	themeChange := false

	theme := ptr.themeComboBox.CurrentText()
	if conf.Conf.Theme != theme {
		conf.Conf.Theme = theme
		themeChange = true
	}

	conf.Conf.ListerPort, _ = strconv.Atoi(ptr.portEdit.Text())
	conf.Conf.HijackDNS = ptr.hijackDNS.IsChecked()

	conf.Conf.TProxy = ptr.tproxyComboBox.CurrentText()

	conf.Conf.DnsServers = ptr.dnsListWidget.Data()

	conf.Conf.IptablesDirectIPList = strings.FieldsFunc(strings.ReplaceAll(ptr.iptablesDirectIPEdit.ToPlainText(), ",", " "), unicode.IsSpace)

	conf.Conf.AdsOutboundTag = ptr.adsComboBox.CurrentText()
	conf.Conf.BTOutboundTag = ptr.btComboBox.CurrentText()
	conf.Conf.IPOutboundTag = ptr.ipComboBox.CurrentText()
	conf.Conf.SiteOutboundTag = ptr.siteComboBox.CurrentText()

	conf.Conf.DomainWhitelist = strings.FieldsFunc(strings.ReplaceAll(ptr.domainWhitelistEdit.ToPlainText(), ",", " "), unicode.IsSpace)
	conf.Conf.DomainBlacklist = strings.FieldsFunc(strings.ReplaceAll(ptr.domainBlacklistEdit.ToPlainText(), ",", " "), unicode.IsSpace)

	conf.Conf.IPWhitelist = strings.FieldsFunc(strings.ReplaceAll(ptr.ipWhitelistEdit.ToPlainText(), ",", " "), unicode.IsSpace)
	conf.Conf.IPBlacklist = strings.FieldsFunc(strings.ReplaceAll(ptr.ipBlacklistEdit.ToPlainText(), ",", " "), unicode.IsSpace)

	conf.Conf.Hosts = ptr.hostTableWidget.Data()
	conf.Conf.Subscriptions = ptr.subscriptionTableWidget.Data()

	if err := conf.Conf.SaveConf(); err != nil {
		widgets.QMessageBox_Information(ptr, "错误", "配置文件保存失败，错误："+err.Error(), widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
	} else {
		if themeChange {
			widgets.QMessageBox_Information(ptr, "提示", "主题修改将在下次启动时生效", widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
		}

		ptr.Close()
	}
}
