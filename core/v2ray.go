package core

import (
	"errors"
	"gitlab.com/xiayesuifeng/v2rayxplus/conf"
	"os/exec"
	"strings"
)

func GetVension() (string, error) {
	cmd := exec.Command("v2ray", "-version")
	out, err := cmd.CombinedOutput()
	if err != nil {
		return "", errors.New("v2ray not exist")
	}

	str := string(out)
	if !strings.Contains(str, "V2Ray") {
		return "", errors.New("v2ray not exist")
	}

	strs := strings.Split(str, " ")

	if len(strs) < 2 {
		return "", errors.New("v2ray not exist")
	}

	return strs[1], nil
}

func StartV2ray() error {
	v2Config, err := conf.ParseV2ray("/etc/v2ray/config.json")
	if err != nil {
		return err
	}

	v2Config.V2rayXPlus.Port = -1
	v2Config.V2rayXPlus.TProxy = "nat"

	for _, config := range v2Config.InboundConfigList {
		if config.Protocol == "dokodemo-door" {
			v2Config.V2rayXPlus.Port = int(*config.Port)
			if config.StreamSetting != nil && config.StreamSetting.SocketSettings != nil {
				if config.StreamSetting.SocketSettings.TProxy == "tproxy" {
					v2Config.V2rayXPlus.TProxy = "mangle"
				}
			}
			break
		}
	}

	for _, config := range v2Config.OutboundConfigList {
		if config.Tag == "dns-out" {
			v2Config.V2rayXPlus.HijackDNS = true
		}
	}

	if v2Config.V2rayXPlus.Port == -1 {
		return errors.New("dokodemo-door port not found")
	}

	InitIpTables()

	if err := AddIpTablesRules(&v2Config.V2rayXPlus); err != nil {
		return err
	}

	return nil
}

func StopV2ray() error {
	StopService("v2ray")

	if err := RemoveIpTablesRules(); err != nil {
		return err
	}

	return nil
}
