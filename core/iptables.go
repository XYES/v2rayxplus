package core

import (
	"bytes"
	"gitlab.com/xiayesuifeng/v2rayxplus/conf"
	"os/exec"
	"strings"
	"text/template"
)

const iptablesRules = `iptables -t {{ .TProxy }} -N V2RAY
iptables -t {{ $.TProxy }} -A V2RAY -d 127.0.0.1/32 -j RETURN
{{- range .IptablesDirectIPList }}
{{- if and $.HijackDNS (notHasSuffix . "/32") }}
iptables -t {{ $.TProxy }} -A V2RAY -d {{ . }} -p tcp -j RETURN
iptables -t {{ $.TProxy }} -A V2RAY -d {{ . }} -p udp ! --dport 53 -j RETURN
{{- else }}
iptables -t {{ $.TProxy }} -A V2RAY -d {{ . }} -j RETURN
{{- end }}
{{- end }}

ip rule add fwmark 1 table 100
ip route add local 0.0.0.0/0 dev lo table 100

iptables -t mangle -N V2RAY_MASK
{{- range .IptablesDirectIPList }}
{{- if and $.HijackDNS (notHasSuffix . "/32") }}
iptables -t mangle -A V2RAY_MASK -d {{ . }} -p tcp -j RETURN
iptables -t mangle -A V2RAY_MASK -d {{ . }} -p udp ! --dport 53 -j RETURN
{{- else }}
iptables -t mangle -A V2RAY_MASK -d {{ . }} -j RETURN
{{- end }}
{{- end }}

{{- if eq .TProxy "nat" }}
iptables -t nat -A V2RAY -p tcp -j RETURN -m mark --mark 0xff
iptables -t nat -A V2RAY -p tcp -j REDIRECT --to-ports {{ .Port }}
iptables -t nat -A PREROUTING -p tcp -j V2RAY
iptables -t nat -A OUTPUT -p tcp -j V2RAY
iptables -t mangle -A V2RAY_MASK -p udp -j TPROXY --on-port {{ .Port }} --tproxy-mark 1
iptables -t mangle -A PREROUTING -p udp -j V2RAY_MASK
{{- else }}
iptables -t mangle -A V2RAY -j RETURN -m mark --mark 0xff
iptables -t mangle -A V2RAY -p udp -j TPROXY --on-ip 127.0.0.1 --on-port {{ .Port }} --tproxy-mark 1
iptables -t mangle -A V2RAY -p tcp -j TPROXY --on-ip 127.0.0.1 --on-port {{ .Port }} --tproxy-mark 1
iptables -t mangle -A PREROUTING -j V2RAY
iptables -t mangle -A V2RAY_MASK -j RETURN -m mark --mark 0xff
iptables -t mangle -A V2RAY_MASK -p udp -j MARK --set-mark 1
iptables -t mangle -A V2RAY_MASK -p tcp -j MARK --set-mark 1
iptables -t mangle -A OUTPUT -j V2RAY_MASK

iptables -t mangle -N DIVERT
iptables -t mangle -A DIVERT -j MARK --set-mark 1
iptables -t mangle -A DIVERT -j ACCEPT
iptables -t mangle -I PREROUTING -p tcp -m socket -j DIVERT
{{- end }}
`

func AddIpTablesRules(v2rayXPlusConfig *conf.V2rayXPlusConfig) error {
	tmpl, err := template.New("iptables").Funcs(template.FuncMap{
		"notHasSuffix": func(s, suffix string) bool {
			return !strings.HasSuffix(s, suffix)
		},
	}).Parse(iptablesRules)
	if err != nil {
		return err
	}

	buffer := bytes.Buffer{}

	if err = tmpl.Execute(&buffer, &v2rayXPlusConfig); err != nil {
		return err
	}

	cmd := exec.Command("bash", "-c", buffer.String())
	return cmd.Run()
}

func RemoveIpTablesRules() error {
	sh := `iptables -t nat -F V2RAY
iptables -t nat -D PREROUTING -p tcp -j V2RAY
iptables -t mangle -D PREROUTING -j V2RAY
iptables -t nat -D OUTPUT -p tcp -j V2RAY
iptables -t nat -X V2RAY
iptables -t mangle -F V2RAY
iptables -t mangle -D PREROUTING -j V2RAY
iptables -t mangle -X V2RAY
iptables -t mangle -F V2RAY_MASK
iptables -t mangle -D PREROUTING -p udp -j V2RAY_MASK
iptables -t mangle -D PREROUTING -j V2RAY_MASK
iptables -t mangle -D OUTPUT -j V2RAY_MASK
iptables -t mangle -X V2RAY_MASK
iptables -t mangle -F DIVERT
iptables -t mangle -D PREROUTING -p tcp -m socket -j DIVERT
iptables -t mangle -X DIVERT
ip rule del fwmark 1 table 100
ip route del local 0.0.0.0/0 dev lo table 100`
	cmd := exec.Command("bash", "-c", sh)
	return cmd.Run()
}
