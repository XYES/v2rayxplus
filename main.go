package main

import (
	"encoding/json"
	"errors"
	"flag"
	"github.com/TheCreeper/go-notify"
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/xiayesuifeng/v2rayxplus/conf"
	"gitlab.com/xiayesuifeng/v2rayxplus/core"
	"gitlab.com/xiayesuifeng/v2rayxplus/helper"
	"gitlab.com/xiayesuifeng/v2rayxplus/subscription"
	"gitlab.com/xiayesuifeng/v2rayxplus/ui"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"syscall"
)

var (
	help  = flag.Bool("help", false, "help")
	start = flag.Bool("start", false, "start v2ray")
	stop  = flag.Bool("stop", false, "stop v2ray")
)

func main() {
	if *help {
		flag.Usage()
		os.Exit(0)
	}

	if *start {
		if err := core.StartV2ray(); err != nil {
			log.Println(err)
			os.Exit(1)
		}
		os.Exit(0)
	}

	if *stop {
		if err := core.StopV2ray(); err != nil {
			log.Println(err)
			os.Exit(1)
		}
		os.Exit(0)
	}

	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

		<-c

		stopHelper()

		os.Exit(0)
	}()

	app := widgets.NewQApplication(len(os.Args), os.Args)
	app.SetApplicationVersion("1.1.3")
	app.SetApplicationName("v2rayXPlus")
	app.SetWindowIcon(gui.NewQIcon5(":/resources/v2rayXPlus-64px.svg"))

	ui.NewMainWindow().Show()

	exitCode := app.Exec()

	stopHelper()

	os.Exit(exitCode)
}

func init() {
	flag.Parse()

	if !*stop && !*start {
		var err error
		conf.ConfigPath, err = getConfPath()
		if err != nil {
			log.Panicln(err)
		}
		conf.V2rayConfigPath = conf.ConfigPath + "/v2ray"
		conf.V2rayDefaultConfigPath = conf.V2rayConfigPath + "/default"

		if _, err := os.Stat(conf.V2rayDefaultConfigPath); err != nil {
			if os.IsNotExist(err) {
				if err := os.MkdirAll(conf.V2rayDefaultConfigPath, 0755); err != nil {
					log.Panicln("config dir create failure")
				}
			}
		}

		if err := parseConfig(); err != nil {
			log.Panicln(err)
		}

		if client, err := helper.NewClient(); err != nil {
			log.Println("create helper client error: ", err)
		} else {
			conf.HelperClient = client
		}

		subscription.InitAdapter()
		go updateSubscription()
	}

	if *start {
		conf.Conf = &conf.Config{}
	}
}

func getConfPath() (string, error) {
	path := os.Getenv("HOME")
	if path == "" {
		return "", errors.New("get home failure")
	}

	return filepath.Join(path, ".config/V2rayXPlus"), nil
}

func parseConfig() error {
	file, err := os.OpenFile(conf.ConfigPath+"/config.json", os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		return err
	}
	defer file.Close()

	conf.Conf = &conf.Config{}
	err = json.NewDecoder(file).Decode(conf.Conf)
	if err != nil {
		if err.Error() == "EOF" {
			conf.Conf.Theme = "light"
			conf.Conf.TProxy = "redirect"
			conf.Conf.ListerPort = 8102
			conf.Conf.DnsServers = []conf.DnsServer{conf.NewDnsServerForAddress("8.8.8.8"), conf.NewDnsServerForAddress("8.8.4.4"), conf.NewDnsServerForAddress("localhost")}
			if err := json.NewEncoder(file).Encode(conf.Conf); err != nil {
				return err
			}
		} else {
			return err
		}
	}

	if conf.Conf.AdsOutboundTag == "" {
		conf.Conf.AdsOutboundTag = "block"
	}

	if conf.Conf.BTOutboundTag == "" {
		conf.Conf.BTOutboundTag = "direct"
	}

	if conf.Conf.IPOutboundTag == "" {
		conf.Conf.IPOutboundTag = "direct"
	}

	if conf.Conf.SiteOutboundTag == "" {
		conf.Conf.SiteOutboundTag = "direct"
	}

	if len(conf.Conf.IptablesDirectIPList) == 0 {
		conf.Conf.IptablesDirectIPList = []string{"0.0.0.0/8", "10.0.0.0/8", "100.64.0.0/10",
			"169.254.0.0/16", "172.16.0.0/12", "192.0.0.0/24", "192.0.2.0/24", "192.88.99.0/24", "192.168.0.0/16",
			"198.18.0.0/15", "198.51.100.0/24", "203.0.113.0/24", "224.0.0.0/4", "240.0.0.0/4", "255.255.255.255/32",
			"::1/128", "fc00::/7", "fe80::/10",
		}
	}

	return nil
}

func updateSubscription() {
	log.Println("Start subscription update")
	for _, s := range conf.Conf.Subscriptions {
		if s.UpdateOnStart {
			ntf := notify.NewNotification("V2rayXPlus 订阅", "开始更新 "+s.Group)
			ntf.Show()
			totalCount, availableCount, err := subscription.UpdateSubscription(s.Group, s.URL, s.Adapter)
			log.Printf("%s: Import Total Count: %d, AvailableCount: %d, Error: %s", s.Group, totalCount, availableCount, err)
			ntf = notify.NewNotification("V2rayXPlus 订阅", s.Group+" 更新完成")
			ntf.Show()
		}
	}
	log.Println("Subscription update completed")
}

func stopHelper() {
	if err := exec.Command("pidof", "v2rayxplus-helper").Run(); err == nil {
		log.Println("stop v2rayxplus-helper")
		if err = conf.HelperClient.StopHelper(); err != nil {
			log.Println("stop v2rayxplus-helper fail, error: ", err)
		}
	}
}
