package conf

import (
	"encoding/json"
	"gitlab.com/xiayesuifeng/v2rayxplus/helper"
	"os"
)

var ConfigPath string
var V2rayConfigPath string
var V2rayDefaultConfigPath string

var Conf *Config
var HelperClient *helper.Client

type Config struct {
	Theme                string            `json:"theme"`
	TProxy               string            `json:"tproxy"`
	ListerPort           int               `json:"lister_port"`
	DnsServers           []DnsServer       `json:"dnsServers"`
	AdsOutboundTag       string            `json:"adsOutboundTag"`
	BTOutboundTag        string            `json:"btOutboundTag"`
	IPOutboundTag        string            `json:"ipOutboundTag"`
	SiteOutboundTag      string            `json:"siteOutboundTag"`
	DomainWhitelist      []string          `json:"domainWhitelist"`
	DomainBlacklist      []string          `json:"domainBlacklist"`
	IPWhitelist          []string          `json:"ipWhitelist"`
	IPBlacklist          []string          `json:"ipBlacklist"`
	IptablesDirectIPList []string          `json:"iptablesDirectIPList"`
	HijackDNS            bool              `json:"hijackDNS"`
	Hosts                map[string]string `json:"hosts"`
	Subscriptions        []Subscription    `json:"subscriptions"`
}

type Subscription struct {
	Group         string `json:"group"`
	URL           string `json:"url"`
	Adapter       string `json:"adapter"`
	UpdateOnStart bool   `json:"updateOnStart"`
}

type V2rayXPlusConfig struct {
	HijackDNS            bool     `json:"-"`
	Port                 int      `json:"-"`
	TProxy               string   `json:"-"`
	IptablesDirectIPList []string `json:"iptablesDirectIPList"`
}

func (c *Config) SaveConf() error {
	file, err := os.OpenFile(ConfigPath+"/config.json", os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}

	if err := json.NewEncoder(file).Encode(c); err != nil {
		return err
	}

	return nil
}
