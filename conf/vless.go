package conf

import "encoding/json"

type VlessAccount struct {
	ID         string `json:"id"`
	Flow       string `json:"flow,omitempty"`
	Level      uint16 `json:"level,omitempty"`
	Encryption string `json:"encryption"`
}

type VlessOutboundTarget struct {
	Address string         `json:"address"`
	Users   []VlessAccount `json:"users"`
	Port    uint16         `json:"port"`
}

type VlessOutboundConfig struct {
	Receivers []*VlessOutboundTarget `json:"vnext"`
}

func NewVlessOutboundConfig(jsonData json.RawMessage) (*VlessOutboundConfig, error) {
	conf := &VlessOutboundConfig{}
	return conf, json.Unmarshal(jsonData, conf)
}
